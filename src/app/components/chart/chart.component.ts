import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ChartService } from 'src/app/components/chart/chart.service';
import { chartData } from './datasource';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ChartComponent implements OnInit {
  public stockchartData: Object[] = [];
  public chartData: Object[] = [];
  public ok: boolean = false;
  public crosshair: Object = {};
  public primaryXAxis: Object = {};
  public primaryYAxis: Object = {};
  public title: string = "";

  constructor(public _chartService: ChartService) {

  }

  ngOnInit(): void {
    this.crosshair = {
      enable: true
    };

    this.primaryXAxis = {
      valueType: 'DateTime',
      crosshairTooltip: { enable: true }
    };
    
    this.primaryYAxis = {
      majorTickLines: { color: 'transparent', width: 0 },
      crosshairTooltip: { enable: true }
    };

    this.title = 'Efficiency of oil-fired power production';
    
    // Data for chart series
    this._chartService.getChart("BILI").subscribe(res => {
      let test = res.chartData as any[];
      this.stockchartData = test.map(r => {
        return {
          date: new Date(r.date),
          open: r.open,
          high: r.high,
          low: r.low,
          close: r.close,
          volume: r.volume
        }
      });

      this.ok = true;
    });
  }
}
