import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { ChartComponent } from './components/chart/chart.component';
import { CategoryService, DateTimeCategoryService, LineSeriesService, StockChartModule, StripLineService } from '@syncfusion/ej2-angular-charts';
import { DateTimeService, LegendService, TooltipService } from '@syncfusion/ej2-angular-charts';
import { DataLabelService, CandleSeriesService } from '@syncfusion/ej2-angular-charts';
import { ChartService } from 'src/app/components/chart/chart.service';

import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    ChartComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    StockChartModule,
    HttpClientModule
  ],
  providers: [
    CategoryService,
    LineSeriesService,
    DateTimeService,
    LegendService,
    TooltipService,
    DataLabelService,
    CandleSeriesService,
    StripLineService,
    DateTimeCategoryService,
    ChartService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
